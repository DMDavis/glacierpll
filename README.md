# GlacierPLL  
  
This tool is an unofficial GoLang version of the incredibly useful icePLL tool from the IceStorm community.  
  
There are no *major* differences between the two, as of writing, but [icePLL](https://github.com/YosysHQ/icestorm) should be considered the source of truth. Problems with this tool should be addressed to this repo, and not to the IceStorm team.

## Build
This project was built using GoLang v1.14.3. You can find the appropriate tutorials to download and install GoLang on the [Go website](https://golang.org/).


#### Commands:
```bash
cd $PROJECT_DIR
go build *.go
```
#### Installation:
You will need to add the resultant "glacierpll" binary to your path. This will be different for each operating system. A symlink from the usr/bin/ folder would be my first suggestion for UNIXish OSes.

#### Dependencies:
There are no dependencies beyond the standard go library. Any dependency resolution will be completed via Go Deps, should it ever be required.


