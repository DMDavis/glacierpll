package main

import (
	"fmt"
	"io"
	"math"
)

var debugTemplate = `
F_PLLIN:  %8.3f MHz (given)
F_PLLOUT: %8.3f MHz (requested)
F_PLLOUT: %8.3f MHz (achieved)

FEEDBACK: %s
F_PFD: %8.3f MHz
F_VCO: %8.3f MHz

DIVR: %2d (4'b%04b)
DIVF: %2d (7'b%07b)
DIVQ: %2d (3'b%03b)

FILTER_RANGE: %d (3'b%03b)
`

func debug(writer io.Writer, inputClk, targetClk, achievedClk float64, simple bool, div pllSettings) error {
	pfd := inputClk / float64(div.R+1)
	if div.inputClk != nil {
		pfd = *div.inputClk / float64(div.R+1)
	}
	vco := pfd * float64(div.F+1)

	filter := getFilter(pfd)

	if *complexMode {
		vco *= math.Exp2(float64(div.Q))
	}

	feedback := "SIMPLE"
	if !simple {
		feedback = "NON_SIMPLE"
	}

	_, err := fmt.Fprintf(writer, debugTemplate,
		inputClk, targetClk, achievedClk,
		feedback, pfd, vco,
		div.R, div.R,
		div.F, div.F,
		div.Q, div.Q,
		filter, filter)

	return err
}

var verilogTemplate = ` /**
 * PLL configuration * 
 * This Verilog module was generated automagically
 * using the glacierpll tool: An unofficial GoLang version 
 * of the icepll tool from the IceStorm project.
 * Use at your own peril.
 * 
 * Given input frequency:      %8.3f MHz
 * Requested output frequency: %8.3f MHz
 * Achieved output frequency:  %8.3f MHz
 **/

module %s(
	input  clock_in,
	output clock_out,
	output locked
	);

SB_PLL40_CORE #(
	.FEEDBACK_PATH("%s"),
	.DIVR(4'b%04b),			//DIVR = %2d
	.DIVF(7'b%07b),		//DIVF = %2d
	.DIVQ(3'b%03b),			//DIVQ = %2d
	.FILTER_RANGE(3'b%03b)	//FILTER_RANGE = %d
	) uut (
	.LOCK(locked),
	.RESETB(1'b1),
	.BYPASS(1'b0),
	.REFERENCECLK(clock_in),
	.PLLOUTCORE(clock_out)
	);

endmodule
`

func moduleize(writer io.Writer, inputClk, targetClk, achievedClk float64, name string, simple bool, div pllSettings) error {
	feedback := "SIMPLE"
	if !simple {
		feedback = "NON_SIMPLE"
	}

	pfd := inputClk / float64(div.R+1)
	if div.inputClk != nil {
		pfd = *div.inputClk / float64(div.R+1)
	}

	filter := getFilter(pfd)

	_, err := fmt.Fprintf(writer, verilogTemplate,
		inputClk, targetClk, achievedClk,
		name,
		feedback,
		div.R, div.R,
		div.F, div.F,
		div.Q, div.Q,
		filter, filter)

	return err
}

var configTemplate = `/**
 * PLL configuration
 *
 * This Verilog header file was generated automagically
 * using the glacierpll tool: An unofficial GoLang version 
 * of the icepll tool from the IceStorm project.
 * It is intended for use with FPGA primitives SB_PLL40_CORE,
 * SB_PLL40_PAD, SB_PLL40_2_PAD, SB_PLL40_2F_CORE or SB_PLL40_2F_PAD.
 * Use at your own peril.
 * 
 * Given input frequency:      %8.3f MHz
 * Requested output frequency: %8.3f MHz
 * Achieved output frequency:  %8.3f MHz
**/

	.FEEDBACK_PATH("%s"),
	.DIVR(4'b%04b),			//DIVR = %2d
	.DIVF(7'b%07b),		//DIVF = %2d
	.DIVQ(3'b%03b),			//DIVQ = %2d
	.FILTER_RANGE(3'b%03b)	//FILTER_RANGE = %d
`

func configure(writer io.Writer, inputClk, targetClk, achievedClk float64, simple bool, div pllSettings) error {
	feedback := "SIMPLE"
	if !simple {
		feedback = "NON_SIMPLE"
	}

	pfd := inputClk / float64(div.R+1)
	if div.inputClk != nil {
		pfd = *div.inputClk / float64(div.R+1)
	}

	filter := getFilter(pfd)

	_, err := fmt.Fprintf(writer, configTemplate,
		inputClk, targetClk, achievedClk,
		feedback,
		div.R, div.R,
		div.F, div.F,
		div.Q, div.Q,
		filter, filter)

	return err
}
