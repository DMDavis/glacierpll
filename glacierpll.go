package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"
)

var (
	moduleSave  = flag.Bool("m", false, "Save PLL config as Verilog module")
	complexMode = flag.Bool("S", false, "Bypass SIMPLE feedback mode")
	setBests    = flag.Bool("b", false, "Get best input frequencies from standard oscillators")
	fileBests   = flag.String("B", "", "Get best input frequencies from file specified")
	fileOutput  = flag.String("f", "", "Save PLL configuration to filename (otherwise writes to stdout)")
	moduleName  = flag.String("n", "pll", "PLL module name (default 'pll')")
	quiet       = flag.Bool("q", false, "Quiets the output from the tool")

	spliter = regexp.MustCompile(`\w+`)
)

type pllSettings struct {
	R, F, Q  int
	inputClk *float64
}

func init() {
	flag.Usage = Usage
}

func Usage() {
	_, _ = fmt.Fprintf(flag.CommandLine.Output(), "%s Usage: [options] [input clk MHz] [desired clk MHz]\n\n", os.Args[0])
	flag.PrintDefaults()
}

func getFloatTableFromFile(filename string) []float64 {
	floatTable := make([]float64, 0, 16)
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("Could not open the requested file", err)
	}
	substrings := spliter.Split(string(bytes), -1)
	for _, substring := range substrings {
		value, err := strconv.ParseFloat(substring, 64)
		if err != nil {
			log.Printf("WARNING: a float from file '%s' was not included due to:\n%s\n", *fileBests, err.Error())
		}
		floatTable = append(floatTable, value)
	}
	return floatTable
}

var frequencyTable = []float64{
	10, 11.0592, 11.2896, 11.7846, 12, 12.288, 12.352, 12.5, 13, 13.5, 13.6, 14.31818, 14.7456, 15, 16, 16.384, 17.2032,
	18.432, 19.2, 19.44, 19.6608, 20, 24, 24.576, 25, 26, 27, 27.12, 28.63636, 28.9, 29.4912, 30, 32, 32.768, 33, 33.206,
	33.333, 35.328, 36, 37.03, 37.4, 38.4, 38.88, 40, 40.95, 40.97, 44, 44.736, 48, 50, 54, 57.692, 60, 64, 65, 66,
	66.666, 68, 70, 72, 75, 76.8, 80, 80.92, 92.16, 96, 98.304, 100, 104, 106.25, 108, 114.285, 120, 122.88, 125,
}

var filterLevels = []float64{
	17, 26, 44, 66, 101,
}

func getFilter(pfd float64) int {
	for i := range filterLevels {
		if pfd < filterLevels[i] {
			return i + 1
		}
	}
	return 6
}

func main() {
	flag.Parse()

	outputFile := os.Stdout
	bestMode := *setBests

	args := flag.Args()

	if len(args) != 2 {
		flag.Usage()
		os.Exit(1)
	}

	input, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		log.Fatal("input frequency must be a number.", err)
	}

	output, err := strconv.ParseFloat(args[1], 64)
	if err != nil {
		log.Fatal("output frequency must be a number.", err)
	}

	if fileBests != nil && len(*fileBests) > 0 {
		bestMode = true
		frequencyTable = getFloatTableFromFile(*fileBests)
	}

	if fileOutput != nil && len(*fileOutput) > 0 {
		outputFile, err = os.OpenFile(*fileOutput, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0644)
		if err != nil {
			log.Fatal("output file specified cannot be opened", err)
		}
	} else {
		silent := true
		quiet = &silent
	}

	clkOut, div, err := performAnalysis(!*complexMode, bestMode, input, output, frequencyTable)
	if err != nil {
		log.Fatal(err)
	}

	if div.inputClk != nil {
		input = *div.inputClk
	}

	if !*quiet {
		_ = debug(log.Writer(), input, output, clkOut, !*complexMode, div)
	}

	if *moduleSave {
		_ = moduleize(outputFile, input, output, clkOut, *moduleName, !*complexMode, div)
	} else {
		_ = configure(outputFile, input, output, clkOut, !*complexMode, div)
	}

	if !*quiet {
		log.Printf("PLL configuration written to %s\n", *fileOutput)
	}
}

func performAnalysis(simple, bestMode bool, in, target float64, freqTable []float64) (clkOut float64, div pllSettings, err error) {
	if !bestMode {
		ok, clkOut, div, err := analyze(simple, in, target)
		if ok {
			return clkOut, div, err
		}
	} else {
		found := false
		for i, freq := range freqTable {
			ok, tmpOut, tmpDiv, err := analyze(!*complexMode, freq, target)
			if err != nil {
				return 0, div, err
			}
			if ok {
				found = true
				if math.Abs(tmpOut-target) < math.Abs(clkOut-target) {
					clkOut = tmpOut
					div = tmpDiv
					div.inputClk = &freqTable[i]
				}
			}
		}
		if found {
			return clkOut, div, nil
		}
	}
	return 0, div, errors.New("no valid DIV configuration found")
}

func analyze(simple bool, clkIn, clkTarget float64) (found bool, bestOut float64, div pllSettings, err error) {
	found = false

	// The documentation in the iCE40 PLL Usage Guide incorrectly lists the
	// maximum value of DIVF as 63, when it is only limited to 63 when using
	// feedback modes other that SIMPLE.
	divfMax := 63
	if simple {
		divfMax = 127
	}

	if clkIn < 10 || clkIn > 133 {
		return found, 0, div, errors.New(fmt.Sprintf("PLL input frequency %.3f MHz is outside range 10MHz - 133MHz", clkIn))
	}

	if clkTarget < 16 || clkTarget > 275 {
		return found, 0, div, errors.New(fmt.Sprintf("PLL output frequency %.3f MHz is outside range 16MHz - 275MHz", clkIn))
	}

	for divr := 0; divr <= 15; divr++ {

		pfd := clkIn / float64(divr+1)
		if pfd < 10 && pfd > 133 {
			continue
		}

		for divf := 0; divf <= divfMax; divf++ {

			if simple {

				vco := pfd * float64(divf+1)
				if vco < 533 || vco > 1066 {
					continue
				}

				for divq := 1; divq <= 6; divq++ {
					fout := vco * math.Exp2(float64(-divq))
					if math.Abs(fout-clkTarget) < math.Abs(bestOut-clkTarget) || !found {
						bestOut = fout
						div.R = divr
						div.F = divf
						div.Q = divq
						found = true
					}
				}
			} else {

				for divq := 1; divq <= 6; divq++ {

					vco := pfd * float64(divf+1) * math.Exp2(float64(divq))
					if vco < 533 || vco > 1066 {
						continue
					}

					fout := vco * math.Exp2(float64(-divq))
					if math.Abs(fout-clkTarget) < math.Abs(bestOut-clkTarget) || !found {
						bestOut = fout
						div.R = divr
						div.F = divf
						div.Q = divq
						found = true
					}
				}
			}
		}
	}

	return found, bestOut, div, nil
}
